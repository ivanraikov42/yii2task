<?php

namespace frontend\controllers;

use common\models\CarSearch;
use Yii;
use common\models\Brand;
use common\models\Model;
use yii\helpers\ArrayHelper;

class CatalogController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new CarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $brand = ArrayHelper::map(Brand::find()->asArray()->all(), 'id', 'brand');
        $model = ArrayHelper::map(Model::find()->asArray()->all(), 'id', 'model');
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'brand' => $brand,
            'model' => $model
        ]);
    }

}
