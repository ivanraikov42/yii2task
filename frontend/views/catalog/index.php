<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$request = Yii::$app->request->get('CarSearch');

$this->title = 'Продажа новых автомобилей '.$brand[$request['brand_id']].' '.$model[$request['model_id']].' в Санкт-Петербурге';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
    	'filterModel' => $searchModel,
        'columns' => [
            'id',
			[
            'attribute'  =>'brand_id',
                'content' => function($data){
                    return $data->brand->brand;
                },
                'filter' => $brand
            ],
            [
            	'attribute'  =>'model_id',
                'content' => function($data){
                    return $data->model->model;
                },
                'filter' => $model
            ],
            [
            'attribute'  =>'engine_type_id',
                'content' => function($data){
                    return $data->engineType->engine_type;
                },
            ],
            [
            'attribute'  =>'wd_id',
                'content' => function($data){
                    return $data->wd->wd;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
