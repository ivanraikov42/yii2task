<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd".
 *
 * @property int $id
 * @property string|null $wd
 *
 * @property Car[] $cars
 */
class WD extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['wd'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wd' => 'Wd',
        ];
    }

    /**
     * Gets query for [[Cars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['wd_id' => 'id']);
    }
}
