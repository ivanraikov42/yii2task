<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "car".
 *
 * @property int $id
 * @property int $model_id
 * @property int $engine_type_id
 * @property int $wd_id
 * @property int $brand_id
 *
 * @property Brand $brand
 * @property EngineType $engineType
 * @property Model $model
 * @property Wd $wd
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'engine_type_id', 'wd_id', 'brand_id'], 'required'],
            [['model_id', 'engine_type_id', 'wd_id', 'brand_id'], 'integer'],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['engine_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => EngineType::className(), 'targetAttribute' => ['engine_type_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => Model::className(), 'targetAttribute' => ['model_id' => 'id']],
            [['wd_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wd::className(), 'targetAttribute' => ['wd_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Модель',
            'engine_type_id' => 'Тип двигателя',
            'wd_id' => 'Тип пивода',
            'brand_id' => 'Марка',
        ];
    }

    /**
     * Gets query for [[Brand]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * Gets query for [[EngineType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEngineType()
    {
        return $this->hasOne(EngineType::className(), ['id' => 'engine_type_id']);
    }

    /**
     * Gets query for [[Model]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Model::className(), ['id' => 'model_id']);
    }

    /**
     * Gets query for [[Wd]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWd()
    {
        return $this->hasOne(Wd::className(), ['id' => 'wd_id']);
    }
}
