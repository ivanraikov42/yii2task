<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Car', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
            'attribute'  =>'model_id',
                'content' => function($data){
                    return $data->model->model;
                },
            ],
            [
            'attribute'  =>'engine_type_id',
                'content' => function($data){
                    return $data->engineType->engine_type;
                },
            ],
            [
            'attribute'  =>'wd_id',
                'content' => function($data){
                    return $data->wd->wd;
                },
            ],
            [
            'attribute'  =>'brand_id',
                'content' => function($data){
                    return $data->brand->brand;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
