<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Car */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($car, 'brand_id')->dropDownList(ArrayHelper::map($brand, 'id', 'brand')) ?>

    <?= $form->field($car, 'model_id')->dropDownList(ArrayHelper::map($model, 'id', 'model')) ?>

    <?= $form->field($car, 'engine_type_id')->dropDownList(ArrayHelper::map($engineType, 'id', 'engine_type')) ?>

    <?= $form->field($car, 'wd_id')->dropDownList(ArrayHelper::map($wd, 'id', 'wd')) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
