<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%wd}}`.
 */
class m200904_072228_create_wd_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%wd}}', [
            'id' => $this->primaryKey(),
            'wd' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%wd}}');
    }
}
