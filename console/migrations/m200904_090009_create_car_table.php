<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%car}}`.
 */
class m200904_090009_create_car_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%car}}', [
            'id' => $this->primaryKey(),
            'model_id' => $this->integer()->notNull(),
            'engine_type_id' => $this->integer()->notNull(),
            'wd_id' => $this->integer()->notNull(),
            'brand_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'fk-car-model_id',
            'car',
            'model_id',
            'model',
            'id',
            'NO ACTION'
        );

        $this->addForeignKey(
            'fk-car-engine_type_id',
            'car',
            'engine_type_id',
            'engine_type',
            'id',
            'NO ACTION'
        );

        $this->addForeignKey(
            'fk-car-wd_id',
            'car',
            'wd_id',
            'wd',
            'id',
            'NO ACTION'
        );

        $this->addForeignKey(
            'fk-car-brand_id',
            'car',
            'brand_id',
            'brand',
            'id',
            'NO ACTION'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-car-model_id',
            'car'
        );

        $this->dropForeignKey(
            'fk-car-engine_type_id',
            'car'
        );

        $this->dropForeignKey(
            'fk-car-wd_id',
            'car'
        );

        $this->dropForeignKey(
            'fk-car-brand_id',
            'car'
        );
        $this->dropTable('{{%car}}');
    }
}
