<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%engine_type}}`.
 */
class m200904_071917_create_engine_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%engine_type}}', [
            'id' => $this->primaryKey(),
            'engine_type' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%engine_type}}');
    }
}
